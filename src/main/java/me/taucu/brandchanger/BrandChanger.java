package me.taucu.brandchanger;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

import dev.simplix.protocolize.api.Protocolize;
import me.taucu.brandchanger.listeners.BrandSendListener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

public class BrandChanger extends Plugin {
    
    private final ConfigurationProvider configProvider = YamlConfiguration.getProvider(YamlConfiguration.class);
    
    final File dataFolder = getDataFolder();
    final File configFile = new File(dataFolder, "config.yml");
    private Configuration config = null;
    
    final BrandSendListener listener = new BrandSendListener();
    
    @Override
    public void onEnable() {
        config = loadConfig();
        listener.setBrand(config.getString("server brand"));
        Protocolize.listenerProvider().registerListener(listener);
    }
    
    @Override
    public void onDisable() {
        Protocolize.listenerProvider().unregisterListener(listener);
    }
    
    public Configuration loadConfig() {
        Configuration internalConfig;
        Configuration config;
        
        try (InputStream is = getResourceAsStream("config.yml")) {
            internalConfig = configProvider.load(is);
        } catch (Exception e) {
            throw new ConfigurationException("failed to load internal config", e);
        }
        
        if (!configFile.exists()) {
            config = regenConfig(configFile, internalConfig);
        } else {
            try {
                config = configProvider.load(configFile, internalConfig);
                double ver = config.getDouble("config version");
                double iver = internalConfig.getDouble("config version");
                if (ver != iver) {
                    if (ver % 1 != iver % 1) {
                        getLogger().warning("config major version is oudated, regenerating");
                        config = regenConfig(configFile, internalConfig);
                    }
                    getLogger().warning("config minor version is outdated. it should be regenerated");
                }
            } catch (IOException e) {
                new ConfigurationException("error while loading config, regenerating", e).printStackTrace();
                config = regenConfig(configFile, internalConfig);
            }
        }
        
        return config;
    }
    
    private Configuration regenConfig(File configFile, Configuration defaults) {
        try {
            dataFolder.mkdirs();
            if (!configFile.exists()) {
                configProvider.save(defaults, configFile);
                return configProvider.load(configFile, defaults);
            }
            for (int i = 0; i < 100; i++) {
                File to = new File(dataFolder, String.format("config.old.%s.yml", i));
                if (!to.exists()) {
                    Files.move(configFile.toPath(), to.toPath());
                    configProvider.save(defaults, configFile);
                    return configProvider.load(configFile, defaults);
                }
            }
            throw new ConfigurationException("unable to move config file, there are too many old configs");
        } catch (Exception e) {
            throw new ConfigurationException("error while regenerating config file", e);
        }
    }
    
}
