package me.taucu.brandchanger;

public class ConfigurationException extends RuntimeException {
    
    private static final long serialVersionUID = 9148075401215306052L;
    
    public ConfigurationException() {}
    
    public ConfigurationException(String msg) {
        super(msg);
    }
    
    public ConfigurationException(String msg, Throwable cause) {
        super(msg, cause);
    }
    
}
