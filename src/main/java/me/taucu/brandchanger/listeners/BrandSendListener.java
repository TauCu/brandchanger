package me.taucu.brandchanger.listeners;

import dev.simplix.protocolize.api.Direction;
import dev.simplix.protocolize.api.listener.AbstractPacketListener;
import dev.simplix.protocolize.api.listener.PacketReceiveEvent;
import dev.simplix.protocolize.api.listener.PacketSendEvent;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import net.md_5.bungee.protocol.packet.PluginMessage;

public class BrandSendListener extends AbstractPacketListener<PluginMessage> {
    
    private byte[] brand = null;
    
    public BrandSendListener() {
        super(PluginMessage.class, Direction.UPSTREAM, 0);
        setBrand("CraftBukkit");
    }
    
    @Override
    public void packetSend(PacketSendEvent<PluginMessage> event) {
        PluginMessage msg = event.packet();
        if (msg != null && "minecraft:brand".equals(msg.getTag())) {
            msg.setData(brand);
        }
    }
    
    @Override
    public void packetReceive(PacketReceiveEvent<PluginMessage> paramPacketReceiveEvent) {}
    
    public void setBrand(String brand) {
        ByteBuf buf = Unpooled.buffer();
        PluginMessage.writeString(brand, buf);
        this.brand = buf.array();
    }
    
}
