BrandChanger is a simple Bungeecord plugin that depends on Protocolize 2.0 or greater

As the name suggests it allows you to change the brand of servers accessed through a proxy running this plugin.

The main use of this being to obfuscate the type of server you are using to make it harder for people to find exploits.